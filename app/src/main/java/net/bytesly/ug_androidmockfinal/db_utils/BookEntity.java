package net.bytesly.ug_androidmockfinal.db_utils;

public class BookEntity {

    private Integer id;
    private String title;
    private String author;
    private String releaseYear;

    public BookEntity() {
    }

    public BookEntity(Integer id, String title, String author, String releaseYear) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.releaseYear = releaseYear;
    }

    public BookEntity(String title, String author, String releaseYear) {
        this.title = title;
        this.author = author;
        this.releaseYear = releaseYear;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    @Override
    public String toString() {
        return "Book {\"" + title + '\"' +
                ", author='" + author + '\'' +
                ", year='" + releaseYear + '\'' +
                '}';
    }
}
