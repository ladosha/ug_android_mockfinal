package net.bytesly.ug_androidmockfinal.db_utils;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class BookDbHelper extends SQLiteOpenHelper  {

    private static final String DATABASE_NAME = "DB_NAME";
    private static final int VERSION = 1;

    private static final String SQL_CREATE_TABLE = "CREATE TABLE " + BookContract.TABLE_NAME + " ("
            + BookContract.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + BookContract.TITLE + " TEXT, "
            + BookContract.AUTHOR + " TEXT, "
            + BookContract.RELEASE_YEAR + " INTEGER)";

    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + BookContract.TABLE_NAME;

    public BookDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }

    public void insert(String title, String author, Integer releaseYear) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(BookContract.TITLE, title);
        contentValues.put(BookContract.AUTHOR, author);
        contentValues.put(BookContract.RELEASE_YEAR, releaseYear);

        getWritableDatabase().insert(BookContract.TABLE_NAME, null, contentValues);
    }

    public List<BookEntity> selectAll() {

        String[] projection = new String[]{
                BookContract.TITLE, BookContract.AUTHOR,
                BookContract.RELEASE_YEAR
        };

        String where = BookContract.ID + " > 0";
        String[] args = new String[]{};

        String ordering = BookContract.ID + " ASC";

        @SuppressLint("Recycle") Cursor cursor = getReadableDatabase().query(
                BookContract.TABLE_NAME,
                projection,
                where,
                args,
                null,
                null,
                ordering
        );

        List<BookEntity> books = new ArrayList<>();

        while (cursor.moveToNext()) {
            books.add(new BookEntity(
                    cursor.getString(cursor.getColumnIndex(BookContract.TITLE)),
                    cursor.getString(cursor.getColumnIndex(BookContract.AUTHOR)),
                    cursor.getString(cursor.getColumnIndex(BookContract.RELEASE_YEAR))
            ));
        }

        return books;

    }

}
