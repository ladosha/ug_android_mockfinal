package net.bytesly.ug_androidmockfinal.fragments_profile;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.bytesly.ug_androidmockfinal.R;
import net.bytesly.ug_androidmockfinal.db_utils.BookDbHelper;


public class HomeFragment extends Fragment {

    EditText editTextBookTitle;
    EditText editTextBookAuthor;
    EditText editTextBookReleaseYear;

    Button buttonAdd;

    BookDbHelper bookDbHelper;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bookDbHelper = new BookDbHelper(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editTextBookTitle = view.findViewById(R.id.editTextBookTitle);
        editTextBookAuthor = view.findViewById(R.id.editTextBookAuthor);
        editTextBookReleaseYear = view.findViewById(R.id.editTextBookReleaseYear);

        buttonAdd = view.findViewById(R.id.buttonAdd);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateFields()) {
                    addBookToDb();
                }
                else {
                    Toast.makeText(getContext(), "Fields are invalid", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addBookToDb() {
        String title = editTextBookTitle.getText().toString();
        String author = editTextBookAuthor.getText().toString();
        Integer releaseYear = Integer.parseInt(editTextBookReleaseYear.getText().toString());

        bookDbHelper.insert(title, author, releaseYear);

        editTextBookTitle.setText("");
        editTextBookAuthor.setText("");
        editTextBookReleaseYear.setText("");

        Toast.makeText(getContext(), "Success!", Toast.LENGTH_SHORT).show();
    }

    private boolean validateFields() {
        boolean check = true;

        if(TextUtils.isEmpty(editTextBookTitle.getText()) ||
            TextUtils.isEmpty(editTextBookAuthor.getText()) ||
            TextUtils.isEmpty(editTextBookReleaseYear.getText())) {
            check = false;
        }

        return check;
    }
}