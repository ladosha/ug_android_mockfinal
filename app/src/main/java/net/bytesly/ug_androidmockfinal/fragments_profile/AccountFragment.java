package net.bytesly.ug_androidmockfinal.fragments_profile;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import net.bytesly.ug_androidmockfinal.MainActivity;
import net.bytesly.ug_androidmockfinal.R;


public class AccountFragment extends Fragment {

    TextView textViewCurrentEmail;
    Button buttonLogOut;

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textViewCurrentEmail = view.findViewById(R.id.textViewCurrentEmail);
        buttonLogOut = view.findViewById(R.id.buttonLogout);

        try {
            textViewCurrentEmail.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());
        }
        catch (Exception e) {

        }

        buttonLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();

                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });


    }
}