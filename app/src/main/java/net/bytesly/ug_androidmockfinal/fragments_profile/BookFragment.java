package net.bytesly.ug_androidmockfinal.fragments_profile;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import net.bytesly.ug_androidmockfinal.R;
import net.bytesly.ug_androidmockfinal.db_utils.BookDbHelper;
import net.bytesly.ug_androidmockfinal.db_utils.BookEntity;

import java.util.ArrayList;
import java.util.List;


public class BookFragment extends Fragment {

    ListView listViewBooks;
    BookDbHelper bookDbHelper;

    public BookFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bookDbHelper = new BookDbHelper(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_book, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listViewBooks = view.findViewById(R.id.listViewBooks);

        List<BookEntity> bookEntities = bookDbHelper.selectAll();

        ArrayAdapter<BookEntity> adapter = new ArrayAdapter<BookEntity>(
                getContext(),
                R.layout.support_simple_spinner_dropdown_item,
                bookEntities);
        listViewBooks.setAdapter(adapter);
    }
}