package net.bytesly.ug_androidmockfinal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavViewMain);

        NavController controller = Navigation.findNavController(this, R.id.nav_host_fragment_main);


        Set<Integer> fragments = new HashSet<>();
        fragments.add(R.id.loginFragment);
        fragments.add(R.id.registerFragment);

        AppBarConfiguration.Builder builder = new AppBarConfiguration.Builder(fragments);

        NavigationUI.setupActionBarWithNavController(this, controller, builder.build());
        NavigationUI.setupWithNavController(bottomNavigationView, controller);
    }



}